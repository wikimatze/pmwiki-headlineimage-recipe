<?php if (!defined('PmWiki')) exit();

$RecipeInfo['HeadlineImage']['Version']='2017-02-24';
$HTMLHeaderFmt[] =
"<link type='text/css' rel='stylesheet' href='\$PubDirUrl/headlineimage/css/style.css'/>
<script type='text/javascript' src='\$PubDirUrl/headlineimage/js/jquery.min.js'></script>
<script type='text/javascript' src='\$PubDirUrl/headlineimage/js/titleblock.jquery.js'></script>";
$HTMLHeaderFmt[] = "
<script type='text/javascript'>
  $(function() {
      $('span img').titleBlock({
          removeTitle: true,
          thefontSize: '20px'
      });
  });
</script>";

Displays the title attribute of an image directly on the image without editing the image with the help of JavaScript.

![Picture of the recipe](http://www.pmwiki.org/pmwiki/uploads/Cookbook/headlineimage.png)


## Questions answered by this recipe

How can I have an amazing headline displayed directly on an image without changing the image and just using simply Wiki
markup?


## Description

This recipe will add the ability to display the title-attribute of an image direct on the page. All you have to do is
to attach an image with the min width of `500 px`.


## Installation

- download from [source][source] or via git `git clone https://bitbucket.org/wikimatze/pmwiki-headlineimage-recipe.git`
- unzip everything
- make the following copy-tasks:
    - put `headlineimage.php` in your cookbook directory (`cp pmwiki-headlineimage-recipe/cookbook/headlineimage.php cookbook/`)
    - put the `headlineimage` folder in your public directory (normally *pub/*) (`cp -R pmwiki-headlineimage-recipe/headlineimage/ pub/`)
- add `include_once("$FarmD/cookbook/headlineimage.php");` in your *config.php*
- make sure to set the `$PubDirUrl'` variable in your *config.php* and put it before including the recipe


## Usage

- use the normal syntax for Attaching an image, but you have to use a special environment:
  - here is an full example:`%headline%Attach:advent.jpg"Headline of the week!"%%`
  - if you have a longer headline, then you must add the break-line-tag <br /\>, e.g. `%headline%Attach:advent.jpg"Headline part one<br />and part two"%%`
- here is the code of the demo page:
  `%headline, width=500px%Attach:vierter_advent.jpg"Headline of the week: It's time for christmas - hope you have got all presents"%%`


## Notes

- I got the idea and code from [css tricks][css tricks].
- The recipe doesn't work with [Mini][mini] (if you use it).


## Comments/Feedback

See [HeadlineImage-Talk][talk] - your comments are welcome there!


## Contact

Feature request, bugs, questions, and so on can be send to <matthias.guenther@wikimatze.de>. If you enjoy the script
please leave your comment under [Headline Users][Headline Users].


## License

This software is licensed under the [MIT license][mit].

© 2011-2017 Matthias Guenther <matthias@wikimatze.de>.

[Headline Users]: http://www.pmwiki.org/wiki/Cookbook/HeadlineImage-Users
[css tricks]: http://css-tricks.com/examples/TypeOverImagePlugin/
[mini]: http://www.pmwiki.org/wiki/Cookbook/Mini/
[mit]: http://en.wikipedia.org/wiki/MIT_License
[source]: https://bitbucket.org/wikimatze/pmwiki-headlineimage-recipe/overview
[talk]: http://www.pmwiki.org/wiki/Cookbook/HeadlineImage-Talk

/* vim: set ts=2 sw=2 textwidth=120: */
